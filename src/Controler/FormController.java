package Controler;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import beans.User;
import business.MyTimerService;
import business.OrdersBusinessInterface;

@ManagedBean
@ViewScoped
public class FormController {
	
	@Inject
	OrdersBusinessInterface service;
	
	@EJB
	MyTimerService timer;
	
	public String onSubmit(User user) {
		
		try {
		//call business service for testing only and to demo CDI
		service.test();
		
		//call timer service (for testing the Programmatic Timer)
		timer.setTimer(10000);
		
		//Gets orders from the database
		getAllOrders();
		insertOrder();
		getAllOrders();
		
		//forward t test response view along with the user managed bean
		FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put("user", user);
		return "TestResponse.xhtml";

		}
		catch(Exception e) {
			return "ErrorPage.xhtml";
		}
	}
	
	public OrdersBusinessInterface getService() {
		return service;
	}
	
	private void insertOrder() {
		Connection conn = null;
		String url = "jdbc:mysql://localhost:3306/testapp";
		String username = "root";
		String password = "9286148990";
		String sql = "INSERT INTO testapp.ORDERS(ID, ORDER_NO, PRODUCT_NAME, PRICE, QUANTITY) VALUES('1', '001122334455', 'This was inserted new', 25.00, 100)";
		try {
			conn = DriverManager.getConnection(url, username, password);
			Statement stat = conn.createStatement();
			stat.executeUpdate(sql);
		}
		catch (SQLException e) {
			e.printStackTrace();
		}
		finally {
			if(conn != null) {
				try {
					conn.close();
				}
				catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	private void getAllOrders() {
		
		Connection conn = null;
		String url = "jdbc:mysql://localhost:3306/testapp";
		String username = "root";
		String password = "root";
		String sql = "Select * FROM testapp.ORDERS";
		try {
			conn = DriverManager.getConnection(url, username,password);
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);
			while (rs.next()) {
				System.out.println(String.format("ID is %d for Product %s at a price of %f", 
						rs.getInt("ID"), 
						rs.getString("PRODUCT_NAME"), 
						rs.getFloat("PRICE")));
			}
			rs.close();
		}
		catch (SQLException e) {
			e.printStackTrace();
		}
		finally {
			if(conn != null) {
				try {
					conn.close();
				}
				catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}
}
