package business;

import java.util.ArrayList;
import java.util.List;
import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.enterprise.inject.Alternative;
import javax.faces.bean.ManagedBean;
import javax.inject.Inject;

import beans.Order;
import data.DataAccessInterface;

@ManagedBean
@Stateless
@Local(OrdersBusinessInterface.class)
@Alternative

public class OrdersBusinessService implements OrdersBusinessInterface {
	@Inject
	DataAccessInterface<Order> OrderDataService;
	private List<Order> orders;

	public List<Order> getOrders() {
		//return all orders from the order data service
		return OrderDataService.findAll();
	}
    public OrdersBusinessService() {

    }

    public void test() {
        // TODO Auto-generated method stub
    	System.out.println("Hello from OrdersBusinessService.test");
    }
    
    public void setOrders(List<Order> orders) {
    	this.orders = orders;
    }

}
